<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\EngineBased\XC5\API\v5_0_0;

use XDev\EM;

/**
 * Class API
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class API extends \XDev\Base\AAPI
{
    const KEY_TYPE_XCN = 2;

    protected $db;

    protected function getXLiteFilename() {
        return \XDev::getSoftwareDir(). \XDev::DS . 'classes' . \XDev::DS . 'XLite.php';
    }

    protected function getTableName($table) {
        return EM::get('Config')->getTablePrefix() . $table;
    }

    public function getVersion() {
        $parser =  new \XDev\Utils\PhpParser();
        $parser->load($this->getXLiteFilename());

        return $parser->parseClassConstValue('XLite', 'XC_VERSION');
    }

    public function getSoftwareName()
    {
        return 'X-Cart ' . $this->getVersion() . ' ' . ($this->getEditionName());
    }

    public function getEditionName()
    {
        $keys = $this->getDb()->query("SELECT xcnPlan, keyData FROM {$this->getTableName('module_keys')} WHERE keyType = " . static::KEY_TYPE_XCN);

        $editionName = 'Trial';

        foreach ($keys as $key) {

            $keyData = unserialize($key['keyData']);

            if (empty($keyData)) {
                continue;
            }

            if (0 !== (int) $key['xcnPlan']) {
                $editionName = $keyData['editionName'];
                break;
            }
        }

        return $editionName;

    }

    public function getSoftwareHttpHost()
    {
        return EM::get('Config')->getHttpHost();
    }

    public function getSoftwareHttpsHost()
    {
        return  EM::get('Config')->getHttpsHost();
    }

    public function getSoftwareWebDir()
    {
        return EM::get('Config')->getWebDir();
    }

    public function getSoftwareStorefrontUrl()
    {
        return $this->getSoftwareWebBaseURL() . '/cart.php';
    }

    public function getSoftwareAdminZoneUrl()
    {
        return $this->getSoftwareWebBaseURL() . '/admin.php';
    }

    public function testHttps()
    {
        $result = file_get_contents($this->getSoftwareWebBaseURL(true) . '/cart.php');

        return $result !== false;
    }

    public function getDb()
    {
        if (!$this->db) {

            if (\XDev::isInitialized()) {
                $this->db = \XDev::getDB();

            } else {
                $config = EM::get('Config');

                $host       = $config->getDbHost();
                $database   = $config->getDbName();
                $username   = $config->getDbUsername();
                $password   = $config->getDbPassword();
                $port       = $config->getDbPort() ? $config->getDbPort() : null;
                $socket     = $config->getDbSocket() ? $config->getDbSocket() : null;

                $this->db = new \XDev\Core\DbConnection($host, $username, $password, $database, $port, $socket);
            }

        }

        return $this->db;
    }
}
