<?php

namespace XDev\Module\XC5\EngineBased\XC5\App\Config\DbDump\v5_0_0;

use XDev\EM;

class DbDump extends \XDev\Config\DbDump
{
    protected function getTablePrefix()
    {
        return EM::get('Config')->getTablePrefix();
    }

    protected function getDefaultConfigData()
    {
        return parent::getDefaultConfigData() + [
            self::PARAM_EXCLUDE_TABLES => [
                $this->getTablePrefix() . 'address_field_value' =>                  ['S' => 'Y'],
                $this->getTablePrefix() . 'form_ids' =>                             ['S' => 'Y'],
                $this->getTablePrefix() . 'import_logs' =>                          ['S' => 'Y'],
                $this->getTablePrefix() . 'orders' =>                               ['S' => 'Y'],
                $this->getTablePrefix() . 'order_coupons' =>                        ['S' => 'Y'],
                $this->getTablePrefix() . 'order_details' =>                        ['S' => 'Y'],
                $this->getTablePrefix() . 'order_history_events' =>                 ['S' => 'Y'],
                $this->getTablePrefix() . 'order_history_event_data' =>             ['S' => 'Y'],
                $this->getTablePrefix() . 'order_items' =>                          ['S' => 'Y'],
                $this->getTablePrefix() . 'order_item_attribute_values' =>          ['S' => 'Y'],
                $this->getTablePrefix() . 'order_item_surcharges' =>                ['S' => 'Y'],
                $this->getTablePrefix() . 'order_surcharges' =>                     ['S' => 'Y'],
                $this->getTablePrefix() . 'order_tracking_number' =>                ['S' => 'Y'],
                $this->getTablePrefix() . 'payment_backend_transactions' =>         ['S' => 'Y'],
                $this->getTablePrefix() . 'payment_backend_transaction_data' =>     ['S' => 'Y'],
                $this->getTablePrefix() . 'xc_product_stats' =>                     ['S' => 'Y'],
                $this->getTablePrefix() . 'profiles' =>                             ['S' => 'Y'],
                $this->getTablePrefix() . 'profile_addresses' =>                    ['S' => 'Y'],
                $this->getTablePrefix() . 'profile_roles' =>                        ['S' => 'Y'],
                $this->getTablePrefix() . 'search_cache' =>                         ['S' => 'Y'],
                $this->getTablePrefix() . 'sessions' =>                             ['S' => 'Y'],
                $this->getTablePrefix() . 'session_cells' =>                        ['S' => 'Y'],
                $this->getTablePrefix() . 'temporary_files' =>                      ['S' => 'Y'],
                $this->getTablePrefix() . 'tmp_vars' =>                             ['S' => 'Y'],
            ],
        ];
    }

}
