<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\EngineBased\XC5\ConfigPatcher\v5_0_0;

/**
 * Class ConfigPatcher
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ConfigPatcher extends \XDev\Utils\RegexPatcher
{
    const REGEXP_VAR_SOCKET             = '/socket\s*?\=\s*[\'\"]([^\'\"]*)[\'\"]\s*/';
    const REGEXP_VAR_DB_USER            = '/username\s*?\=\s*[\'\"]([^\'\"]*)[\'\"]\s*/';
    const REGEXP_VAR_DB_PASS            = '/password\s*?\=\s*[\'\"]([^\'\"]*)[\'\"]\s*/';
    const REGEXP_VAR_AUTH_CODE          = '/auth_code\s*?\=\s*[\'\"]([^\'\"]*)[\'\"]\s*/';
    const REGEXP_VAR_XC_SHARED_SECRET   = '/shared_secret_key\s*?\=\s*[\'\"]([^\'\"]*)[\'\"]\s*/';

    public function applyPatchClearSecureData()
    {
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_SOCKET, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_DB_USER, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_DB_PASS, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_AUTH_CODE, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SHARED_SECRET, '');
    }

    public function applyPatchGenerateSecureData($blowfish)
    {
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_AUTH_CODE, substr(md5($blowfish), 0, 4));
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SHARED_SECRET, md5($blowfish));
    }

}
