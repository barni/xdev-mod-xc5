<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\EngineBased\XC5\Config\v5_0_0;

/**
 * Class Config
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Config {
    static $FILENAME_CONFIG = 'etc/config.php';
    static $FILENAME_CONFIG_LOCAL = 'etc/config.local.php';

    private $configLocalExists = false;

    private $config;
    private $configLocal;

    public function __construct() {
        $this->configLocalExists = file_exists($this->getConfigLocalFilename());
    }

    public function getConfigFilename() {
        return \XDev::getSoftwareDir() . \XDev::DS . self::$FILENAME_CONFIG;
    }

    public function getConfigLocalFilename() {
        return \XDev::getSoftwareDir() . \XDev::DS  . self::$FILENAME_CONFIG_LOCAL;
    }

    public function getVar($section, $name) {

        if ($this->configLocalExists){
            if (!$this->configLocal) {
                $this->configLocal = parse_ini_file($this->getConfigLocalFilename(), true);;
            }

            $result = isset($this->configLocal[$section]) && isset($this->configLocal[$section][$name]) ? $this->configLocal[$section][$name] : NULL;

            if ($result !== NULL) {
                return $result;
            }
        }

        if (!$this->config) {
            $this->config = parse_ini_file($this->getConfigFilename(), true);;
        }

        $result =  isset($this->config[$section]) && isset($this->config[$section][$name]) ? $this->config[$section][$name] : NULL;

        if ($result !== NULL) {
            return $result;

        } else {
            throw new \Exception("No variable $section\['$name'\] found in the configuration file");
        }

    }

    public function getHttpHost() {
        return $this->getVar('host_details', 'http_host');
    }

    public function getHttpsHost() {
        return $this->getVar('host_details', 'https_host');
    }

    public function getWebDir() {
        return $this->getVar('host_details', 'web_dir');
    }

    public function getDbHost() {
        return $this->getVar('database_details', 'hostspec');
    }

    public function getDbName() {
        return $this->getVar('database_details', 'database');
    }

    public function getDbPort() {
        return $this->getVar('database_details', 'port');
    }

    public function getDbSocket() {
        return $this->getVar('database_details', 'socket');
    }

    public function getDbUsername() {
        return $this->getVar('database_details', 'username');
    }

    public function getDbPassword() {
        return $this->getVar('database_details', 'password');
    }

    public function getTablePrefix() {
        return $this->getVar('database_details', 'table_prefix');
    }

}
