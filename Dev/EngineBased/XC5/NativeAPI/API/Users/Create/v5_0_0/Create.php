<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC5\Dev\EngineBased\XC5\NativeAPI\API\Users\Create\v5_0_0;

use XDev\Core\JsonAPI;

/**
 *
 * @author Paul Gavrilenko <barni@x-cart.com>
 */
class Create
{
    const TYPE_ADMIN        = 'A';
    const TYPE_CUSTOMER     = 'C';
    const STATUS_ENABLED    = 'E';
    const STATUS_DISABLED   = 'D';

    protected $params;
    protected $createdProfile;

    public function process($params = [])
    {
        $this->params = $params;

        $this->checkIfProfileExists();

        $this->createProfile();

        $this->createAddresses();

        JsonAPI::setResult([
            'user' => [
                'id' => $this->getCreatedProfile()->getProfileId(),
                'login' => $this->params['login'],
                'password' => $this->params['password'],
            ]
        ]);
    }

    protected function getProfileFields()
    {
        return [
            'login',
            'usertype',
            'status',
            'password',
            'referer',
        ];
    }

    protected function getProfileRequiredParams()
    {
        return [
            'login',
            'usertype',
            'password'
        ];
    }

    protected function getAddressFields() {

        return [
            'title',
            'firstname',
            'lastname',
            'street',
            'city',
            'county',
            'state',
            'country',
            'zipcode',
            'phone',
            'fax',
        ];

    }
    protected function checkIfProfileExists()
    {
        $profile = \XLite\Core\Database::getRepo('XLite\Model\Profile')->findOneBy(['login' => $this->params['login']]);

        if ($profile) {
            $password = $this->getDecryptedPasswordForDisplay($profile->getPassword());

            JsonAPI::addWarning("User {$profile->getLogin()} / $password already exists!");

            exit;
        }
    }

    protected function getCreatedProfile()
    {
        return $this->createdProfile;
    }

    protected function createProfile()
    {
        $this->checkProfileParams();

        $profile = new \XLite\Model\Profile;

        foreach ($this->prepareProfileFields() as $k => $v) {
            $profile->{'set' . $k}($v);
        }

        $em = \XLite\Core\Database::getEM();

        $profile->setPassword($this->getCryptedPassword());

        if ($this->params['usertype'] == static::TYPE_ADMIN) {
            $profile->access_level = \XLite\Core\Auth::getInstance()->getAdminAccessLevel();
            $rootRole = \XLite\Core\Database::getRepo('XLite\Model\Role')->findOneRoot();

            $profile->addRoles($rootRole);
            $rootRole->addProfiles($profile);
        }

        $profile->create();

        $this->createdProfile = $profile;

    }

    protected function checkProfileParams()
    {
        $has_errors = false;

        foreach ($this->getProfileRequiredParams() as $param) {
            if (!isset($this->params[$param])) {
                JsonAPI::addError(sprintf('Param \'%s\' is required', $param));
                $has_errors = true;
            }
        }

        if ($has_errors) {
            exit;
        }

        foreach ($this->params as $param => $param_value) {
            $method_name = 'checkProfileParam' . ucfirst($param);

            if (method_exists($this, $method_name)) {
                $result = $this->{$method_name}($param_value);

                if (!$result) {
                    JsonAPI::addError(sprintf('Invalid param value \'%s\' for %s', $param_value, $param));

                    exit;
                }
            }
        }
    }

    protected function prepareProfileFields()
    {

        $result = [];

        foreach ($this->getProfileFields() as $field)
        {
            if (isset($this->params[$field])) {
                $result[$field] = $this->params[$field];

                $method_name = 'prepareProfileField' . ucfirst($field);

                if (method_exists($this, $method_name)) {
                    $result[$field] = $this->{$method_name}($result[$field]);
                }
            }

        }

        $result['password'] = $this->getCryptedPassword();

        return $result;

    }

    protected function getCryptedPassword()
    {
        return \XLite\Core\Auth::encryptPassword($this->params['password']);
    }

    protected function getDecryptedPasswordForDisplay($password) {
        return substr($password, 0, 3) . '...' . substr($password, strlen($password) - 3, 3);
    }

    protected function createAddresses()
    {
        $profile = $this->getCreatedProfile();

        $billingAddress = $this->getAddress('b');

        if ($billingAddress) {
            $billingAddress->setProfile($profile);

            $profile->addAddresses($billingAddress);

            \XLite\Core\Database::getEM()->persist($billingAddress);
        }

        $shippingAddress = $this->getAddress('s');

        if ($shippingAddress) {
            $shippingAddress->setProfile($profile);

            $profile->addAddresses($shippingAddress);

            \XLite\Core\Database::getEM()->persist($shippingAddress);
        }

        \XLite\Core\Database::getEM()->flush();
    }

    protected function getAddress($type)
    {
        $addressFields = [];

        foreach ($this->getAddressFields() as $field)
        {
            $param_name = $type . '_' . $field;

            if (isset($this->params[$param_name])) {

                $addressFields[$field] = $this->params[$param_name];

            }

        }

        if ($addressFields) {

            $address = new \XLite\Model\Address;

            foreach ($addressFields as $k => $v) {
                $method_name = 'setAddress' . ucfirst($k);

                if (method_exists($this, $method_name)) {
                    $this->{$method_name}($address, $addressFields, $v);

                } else {
                    $method_name = 'set' . ucfirst($k);
                    $address->{$method_name}($v);
                }

            }

            if ($type == 'b') {
                $address->setIsBilling(true);
                $address->setIsShipping(false);

            } else {
                $address->setIsBilling(false);
                $address->setIsShipping(true);
            }

            $address->setIsWork(true);

            if (!$this->params['ship2diff']) {
                $address->setIsBilling(true);
                $address->setIsShipping(true);
            }

            return $address;
        }

        return false;
    }

    protected function setAddressCountry($address, array $addressFields, $v)
    {
        $country = \XLite\Core\Database::getRepo('XLite\Model\Country')
            ->findOneBy(['code' => $v])
        ;

        $address->setCountry($country);

        $state = \XLite\Core\Database::getRepo('XLite\Model\State')
            ->findOneBy(['country' => $country, 'code' => $addressFields['state']])
        ;

        $address->setState($state);
    }

    protected function setAddressState($address, array $addressFields, $v)
    {
       return true;
    }

}
